# Ember-jpa-adapter

This README outlines the details of collaborating on this Ember addon.

## Installation

* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).

##To Use
Add in your package.json:

"ember-jpa-adapter":"git+ssh://git@bitbucket.org/maxicredito/ember-jpa-adapter#master"

**Needs the user ssh public key is allowed in bitbucket to download de dependency.**

```javascript
#in app/serializers/application.js
import JpaSerializer from 'ember-jpa-adapter/serializers/jpa';

export default JpaSerializer;
```

```javascript
#in app/adapters/application.js
import JpaAdapter from 'ember-jpa-adapter/adapters/jpa';

export default JpaAdapter;
```

Extract Links from list, example:
```javascript
//in your controller
links: function(){
  let links = this.get('model.meta.links') || [];
  return links;
}.property('model.meta.links'),


```handlebars
{{!in yor template}}
{{#each link in links}}
  {{link.rel}}
{{/each}}
```

To extract links in your model:
```javascript
//add attribute in your model
links: DS.attr('raw')

```handlebars
{{!and iterate in your template}}
{{#each link in model.links}}
  {{!the attributes is like your JSON this case: [{rel:'abc'}] }}
  {{link.rel}}
{{/each}}
```
