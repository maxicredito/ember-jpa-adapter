import DS from 'ember-data';
import Ember from 'ember';

const {
  pluralize,
  camelize
} = Ember.String;

export default DS.RESTAdapter.extend({
  defaultSerializer: '-jpa',

  pathForType(type) {
    return camelize(pluralize(type));
  },

  isInvalid(status) {
    return status  >= 400 && status < 500;
  },

  handleResponse(status, headers, payload) {
    if (this.isInvalid(status, headers, payload)) {
      //transform in boring json-api errors
      let errors = DS.errorsHashToArray(payload.errors);
      return new DS.InvalidError(errors);
    } else {
      return this._super(status, headers, payload);
    }
  }
});
