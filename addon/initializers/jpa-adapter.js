
import JPASerializer from 'ember-jpa-adapter/serializers/jpa';
import JPAAdapter from 'ember-jpa-adapter/adapters/jpa';

export function initialize(registry) {
  registry.register('adapter:-jpa', JPAAdapter);
  registry.register('serializer:-jpa', JPASerializer);
}

export default {
  name: 'jpa-adapter',
  after: 'store',
  initialize: initialize
};
