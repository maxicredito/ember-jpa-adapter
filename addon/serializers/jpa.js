import Ember from 'ember';
import DS from 'ember-data';

export default DS.RESTSerializer.extend({

  serialize(snapshot, options) {
    options = options || {};
    options.includeId = true;
    return this._super.apply(this, [snapshot, options]);
  },
  /**
    Override relationship names to not appends "_id" or "_ids" when serializing
    relationship keys.
    @method keyForRelationship
    @param {String} key
    @param {String} kind
    @return String
  */
  keyForRelationship(rawKey) {
    return rawKey;
  },
  /**
    Override to not convert camelCased attributes to underscored when serializing.
    @method keyForAttribute
    @param {String} attribute
    @return String
  */
  keyForAttribute(attr) {
    return attr;
  },

  /**
    The `normalizeResponse` method is overrided do set links in meta.links.
    http://stackoverflow.com/questions/32336710/how-to-use-jsonapi-pagination-with-ember-data-1-13/32455212#32455212
    @method normalizeResponse
    @param {DS.Store} store
    @param {DS.Model} primaryModelClass
    @param {Object} payload
    @param {String|Number} id
    @param {String} requestType
    @return {Object} JSON-API Document
  */
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let links = payload.links || {};
    delete payload.links;
    let response = this._super(store, primaryModelClass, payload, id, requestType) || {};
    if(!Ember.get(response, 'meta')) {
      Ember.set(response, 'meta', Ember.Object.create());
    }
    Ember.set(response, 'meta.links', links);
    return response;
  }
});
