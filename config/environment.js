'use strict';

module.exports = function(/* environment, appConfig */) {
  return {
    APP: {
      defaultLocale: 'pt-br'
    },
    contentSecurityPolicy:{
      'default-src': "'none'",
      'script-src': "'self' 'unsafe-eval'",
      'font-src': "'self'",
      'connect-src': "'self' http://localhost:4200",
      'img-src': "'self'",
      'style-src': "'self' 'unsafe-inline'",
      'media-src': "'self'"
    }
  };
};
