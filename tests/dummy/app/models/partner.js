import DS from 'ember-data';

var Partner = DS.Model.extend({
  firstName: DS.attr('string'),
  accounts: DS.hasMany('account', { async: true }),
  links: DS.attr('raw')
});

Partner.reopenClass({
  FIXTURES: [
    { id: 1, firstName: 'José', accounts: [1]},
    { id: 2, firstName: 'Maria', accounts: [2] }
  ]
});

export default Partner;
