import JpaSerializer from 'ember-jpa-adapter/serializers/jpa';
import DS from 'ember-data';

export default JpaSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
    accounts: {embedded: 'always'},
    links: {serialize: false}
  }
});