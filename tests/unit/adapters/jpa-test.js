import { moduleFor, test } from 'ember-qunit';
import DS from 'ember-data';

moduleFor('adapter:application', 'JpaAdapter');


test("test422", function(assert) {
  var adapter = this.subject();
  var json = [{name: ['This field cannot be blank.']}];
  var error = new DS.InvalidError(json);
  var payload = {errors: json};
  var response = adapter.handleResponse(422, {}, payload);
  assert.equal(response, error.toString());
  assert.equal(JSON.stringify(error.errors), JSON.stringify(json));
  console.log(JSON.stringify(error));
});

test("testOk", function(assert) {
  var adapter = this.subject();
  var payload = {};
  var response = adapter.handleResponse(201, {}, payload);
  assert.ok(!(response instanceof DS.InvalidError));
});

test("test401", function(assert) {
  var adapter = this.subject();
  var json = [{error: "usuário e/ou senha inválidos"}];

  var error = new DS.InvalidError(json);
  var payload = {errors: json};
  var response = adapter.handleResponse(401, {}, payload);
  assert.equal(response, error.toString());
});

test("test path for type", function(assert) {
  var adapter = this.subject();
  assert.equal('pessoaFisicas', adapter.pathForType('pessoa-fisica'));
  assert.equal('pessoaFisicas', adapter.pathForType('pessoa-fisicas'));
  assert.equal('pessoaFisicas', adapter.pathForType('pessoaFisica'));
});
