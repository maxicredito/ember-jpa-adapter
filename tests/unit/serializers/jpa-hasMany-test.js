import Ember from 'ember';
import {
  moduleForModel,
  test
} from 'ember-qunit';

var store;

moduleForModel('partner', 'JpaSerializer', {
  needs: [
    'model:account',
    'serializer:application',
    'serializer:partner',
    'transform:raw'
  ],

  setup: function () {
    store = this.store();
  }
});

test('embedded hasMany', function() {
  var account;
  var partner;
  const number = '1234586789';
  const firstName = 'Alex';

  Ember.run(function() {
    partner = store.createRecord('partner', {firstName: firstName});
    account = store.createRecord('account', {
      number: number
    });
    partner.get('accounts').pushObject(account);
  });

  var result = partner.serialize();

  equal(result.firstName, firstName);
  equal(result.accounts[0].number, number);
});