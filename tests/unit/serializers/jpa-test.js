import Ember from 'ember';
import {
  moduleForModel,
  test
} from 'ember-qunit';

var store;

moduleForModel('partner', 'JpaSerializer', {
  needs: [
    'model:account',
    'serializer:application',
    'serializer:account',
    'transform:raw'
  ],

  setup: function () {
    store = this.store();
  }
});

test('embedded belongsTo', function() {
  var account;
  var partner;
  const number = '1234586789';
  const firstName = 'Alex';

  Ember.run(function() {
    partner = store.createRecord('partner', {firstName: firstName});
    account = store.createRecord('account', {
      partner : partner,
      number: number
    });
  });

  var result = account.serialize();

  equal(result.partner.firstName, firstName);
  equal(result.number, number);
});

test('keyForRelationship', function() {
  var serializer = store.serializerFor('partner');
  var result = serializer.keyForRelationship('projectManagers', 'hasMany');
  equal(result, 'projectManagers');
});

test('keyForAttribute', function() {
  var serializer = store.serializerFor('partner');
  var result = serializer.keyForAttribute('firstName');
  equal(result, 'firstName');
});

test('seriliazeId', function() {
  var object;

  Ember.run(function() {
    object = store.createRecord('partner', {firstName: "Alex", id: "124" });
  });

  var result = object.serialize();

  equal('Alex', result.firstName);
  equal('124', result.id);
});

test('extract links', function() {
  var serializer = store.serializerFor('partner');
  var json = {
    partners: [{firstName: "Alex", id: "124" }],
    links: [{rel: 'new'}]
  };
  var meta = serializer.normalizeResponse(store, 'partner', json);

  equal(JSON.stringify(meta), '{"meta":{"links":[{"rel":"new"}]}}');
});

test('extract raw', function() {
  var serializer = store.serializerFor('partner');
  var type = store.modelFor('partner');

  var json = {
    partners: [{firstName: "Alex", id: "124", links: [{rel: 'new'}]}]
  };

  var result = serializer.normalizeSingleResponse(store, type, json);

  equal(result.data.attributes.links[0].rel, 'new');
});


