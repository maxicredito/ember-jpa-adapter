import {
  moduleFor,
  test
} from 'ember-qunit';

moduleFor('transform:raw', {
  // Specify the other units that are required for this test.
  // needs: ['serializer:foo']
});

// Replace this with your real tests.
test('serialize', function() {
  var transform = this.subject();
  equal(1, transform.serialize(1));
});

// Replace this with your real tests.
test('deserialize', function() {
  var transform = this.subject();
  equal(1, transform.deserialize(1));
});
